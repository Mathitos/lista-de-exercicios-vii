
def NeighborJoining(matrix):
	size = len(matrix)
	if size <2:
		print ("done")
		return;
	totalDistance = [0 for x in range(size)]
	matrixNJ = [[0 for x in range(size-1)] for y in range(size-1)]
	
	for i in range(1, size -1):
		for j in range(1, (size-1)):
			totalDistance[i] = totalDistance[i] + matrix[i,j]	

	for i in range(0, size -1):
		for j in range((i+1),(size-1)):
			if i != j:
				matrixNJ[i,j]= (size - 2)*matrix[i,j] - totalDistance[i]- totalDistance[j]
			else:
				matrixNJ[i,j]= 0;
	
	
	minI = 0
	minJ = 0
	
	for i in range(0, size -1):
		for j in range((i+1), size -1):
			if matrixNJ[i,j] < matrixNJ[minI, minJ]:
				minI = i
				minJ = j
	
	delta = (totalDistance[minI]-totalDistance[minJ])/(size - 2)
	limbLenghtI = (1/2)*(matrix[minI,minJ] + delta)
	limbLenghtJ = (1/2)*(matrix[minI,minJ] - delta)
	print (limbLenghtI,limbLenghtJ)
	
	newMatrix = [[0 for x in range(size-2)] for y in range(size-2)]
	jumpI = 0
	jumpJ = 0
	for i in range(0,(size-3)):
		if ((i == minI) | (i == minJ)):
			jumpI += 1
		
		for j in range(0, (size-3)):
			if ((j == minI) | (j == minJ)):
				jumpJ += 1
			
			newMatrix[i,j] = matrix[(i+jumpI),(j+jumpJ)]
	
	for i in range( 0,(size-2)):
		newMatrix[(size-2),i] = (matrix[minI,i]+matrix[minJ,i]-matrix[minI,minJ])/2
		newMatrix[i, (size-2)] = (matrix[minI,i]+matrix[minJ,i]-matrix[minI,minJ])/2
		
	NeighborJoining(newMatrix)
	
	
	
	
		